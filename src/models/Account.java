package models;

public class Account {

    private int accountId;
    private int accountNumber;
    private AccountType accountType;
    private double balance;

    public Account(int accountId, int accountNumber, AccountType accountType, double balance) {
        this.accountId = accountId;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.balance = balance;
    }

    public Account() {
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }


    @Override
    public String toString() {
        return "\n  {" +
                "Id: " + accountId +
                ", número: " + accountNumber +
                ", tipo: " + accountType +
                ", saldo: " + balance +
                "}";
    }
}
