package models;

import java.util.Arrays;

public class User {

    private int positionAccount;

    private int userId;
    private String name;
    private String surname;
    private Account[] accounts;

    public User(int userId, String name, String surname, Account[] accounts) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
        this.accounts = accounts;
    }

    public User() {
        int NUMBER_ACCOUNTS = 3;
        accounts = new Account[NUMBER_ACCOUNTS];
        positionAccount = 0;
        initArrayAccounts();
    }

    public void initArrayAccounts(){
        Arrays.fill(accounts, new Account());
    }

    public void addAccount(Account account){
        account.setAccountId(positionAccount);
        accounts[positionAccount] = account;
        positionAccount++;
    }

    public double[] getSumAccounts(){
        double currentAccount = 0, savingsAccount = 0;

        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getAccountType() == AccountType.CuentaCorriente){
                currentAccount += accounts[i].getBalance();
            } else if (accounts[i].getAccountType() == AccountType.CuentaAhorro){
                savingsAccount += accounts[i].getBalance();
            }
        }

        return new double[]{currentAccount, savingsAccount};
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Account[] getAccounts() {
        return accounts;
    }

    public void setAccounts(Account[] accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return  "\nusuario [" + userId + "]{" +
                "\n Nombres: " + name +
                "\n Apellidos: " + surname +
                "\n Cuentas: " + Arrays.toString(accounts) +
                '}';
    }
}
