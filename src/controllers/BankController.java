package controllers;

import models.Account;
import models.AccountType;
import models.User;

public class BankController {

    private int positionUser;

    private final User[] users;

    public BankController() {
        int NUMBER_USERS = 10;
        users = new User[NUMBER_USERS];
        positionUser = 6;
        initArrayUsers();
    }

    public void addUser(User user) {
        user.setUserId(positionUser);
        users[positionUser] = user;
        positionUser++;
    }

    public AccountType getMaxAccountBalance() {
        try {
            double currentAccount = 0, savingAccount = 0;

            for (int i = 0; i < positionUser; i++) {
                double[] sum = users[i].getSumAccounts();
                currentAccount += sum[0];
                savingAccount += sum[1];
            }
            return getMax(currentAccount, savingAccount);
        } catch (NullPointerException e) {
            System.out.println("  No hay usuarios en el banco");
        }
        return null;
    }

    public AccountType getMax(double currentAccount, double savingAccount) {
        AccountType accountType;
        if (currentAccount >= savingAccount) {
            accountType = AccountType.CuentaCorriente;
        } else {
            accountType = AccountType.CuentaAhorro;
        }
        return accountType;
    }

    public User getMinUserBalanceSavingAccount() {
        try {
            double[] initialSum = users[0].getSumAccounts();

            User userMinBalance = users[0];
            double minUserBalance = initialSum[1];
            for (int i = 1; i < positionUser; i++) {
                double[] sum = users[i].getSumAccounts();

                if (sum[1] < minUserBalance) {
                    minUserBalance = sum[1];
                    userMinBalance = users[i];
                }
            }
            return userMinBalance;
        } catch (NullPointerException ignored) {
        }
        return new User();
    }

    public User getMaxUserBalanceCurrentAccount() {
        try {
            double[] initialSum = users[0].getSumAccounts();

            User userMaxBalance = users[0];
            double maxUserBalance = initialSum[0];
            for (int i = 1; i < positionUser; i++) {
                double[] sum = users[i].getSumAccounts();

                if (sum[1] > maxUserBalance) {
                    maxUserBalance = sum[0];
                    userMaxBalance = users[i];
                }
            }
            return userMaxBalance;
        } catch (NullPointerException ignored) {
        }
        return new User();
    }

    public void sortInsertionMethodUsers() {
        try {
            for (int i = 1; i < positionUser; i++) {
                User aux = users[i];
                for (int j = i - 1; j >= 0; j--) {
                    assert users[j] != null;
                    if (aux.getName().charAt(0) < users[j].getName().charAt(0)) {
                        users[j + 1] = users[j];
                        users[j] = aux;
                    }
                }
            }
            printUsers();
        } catch (NullPointerException ignored) {
        }
    }

    public void printUsers() {
        try {
            System.out.print(" ");
            for (int i = 0; i < positionUser; i++) {
                System.out.print(users[i] + " \n");
            }
        } catch (NullPointerException e) {
            System.out.println("  ¡No hay usuarios en la lista!");
        }
    }

    public void initArrayUsers() {
        users[0] = new User(0, "Nicolás", "Pérez",
                new Account[]{new Account(0, 12542, AccountType.CuentaAhorro, 500000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 450000),
                        new Account(0, 12542, AccountType.CuentaCorriente, 366000)});

        users[1] = new User(1, "Anna", "Ortega",
                new Account[]{new Account(0, 12542, AccountType.CuentaCorriente, 800000),
                        new Account(0, 12542, AccountType.CuentaCorriente, 355000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 45625)});

        users[2] = new User(2, "Camila", "Montenegro",
                new Account[]{new Account(0, 12542, AccountType.CuentaAhorro, 485000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 125000),
                        new Account(0, 12542, AccountType.CuentaCorriente, 182000)});

        users[3] = new User(3, "Vanessa", "Gomez",
                new Account[]{new Account(0, 12542, AccountType.CuentaCorriente, 569000),
                        new Account(0, 12542, AccountType.CuentaCorriente, 601000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 421000)});

        users[4] = new User(4, "Cristian", "Moreno",
                new Account[]{new Account(0, 12542, AccountType.CuentaAhorro, 20000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 302500),
                        new Account(0, 12542, AccountType.CuentaCorriente, 396000)});

        users[5] = new User(5, "Santiago", "Alfonso",
                new Account[]{new Account(0, 12542, AccountType.CuentaCorriente, 473000),
                        new Account(0, 12542, AccountType.CuentaCorriente, 298000),
                        new Account(0, 12542, AccountType.CuentaAhorro, 337000)});
    }
}
