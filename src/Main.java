import controllers.BankController;
import models.Account;
import models.AccountType;
import models.User;
import utiliy.Keyboard;

public class Main {

    public static Keyboard keyboard = new Keyboard();
    public static BankController bankController = new BankController();

    public static void main(String[] args) {
        Main main = new Main();

        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.addUser();
                case 2 -> main.getMinBalanceUser();
                case 3 -> main.getMaxBalanceUser();
                case 4 -> main.sortArrayUsers();
                case 5 -> main.getMaxAccountBalance();
                case 6 -> main.printUsers();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔══════════════════════════════════════════════════════════════╗");
        System.out.println("╠-------------------------SantotoBank--------------------------╣");
        System.out.println("║══════════════════════════════════════════════════════════════║");
        System.out.println("║   1. Agregar usuario                                         ║");
        System.out.println("║   2. Ver usuario con menor saldo de cuenta de ahorros        ║");
        System.out.println("║   3. Ver usuario con mayor saldo de cuenta corriente         ║");
        System.out.println("║   4. Ordenar alfabéticamente los usuarios                    ║");
        System.out.println("║   5. Obtener el tipo de cuenta con mayor saldo               ║");
        System.out.println("║   6. Imprimir usuarios                                       ║");
        System.out.println("║   0. Salir                                                   ║");
        System.out.println("╚══════════════════════════════════════════════════════════════╝");
    }

    public void addUser() {
        User user = new User();
        System.out.print("\n Ingrese el nombre del usuario: ");
        user.setName(keyboard.readLine());
        System.out.print("\n Ingrese los apellidos del usuario: ");
        user.setSurname(keyboard.readLine());
        user.setAccounts(getUserAccounts());

        bankController.addUser(user);
    }

    public Account[] getUserAccounts() {
        int numberAccounts = keyboard.readValidPositiveInteger("\n Ingrese el número de cuentas que posee el usuario: ");
        Account[] accounts = new Account[numberAccounts];
        if (numberAccounts <= 3) {
            for (int i = 0; i < numberAccounts; i++) {
                accounts[i] = readAccount(i);
            }
            return accounts;
        } else {
            System.out.println(" ¡El usuario no puede tener más de tres cuentas asociadas!");
        }
        return null;
    }

    private Account readAccount(int position) {
        Account account = new Account();
        account.setAccountNumber(keyboard.readValidPositiveInteger(
                "\n Ingrese el número de la cuenta " + (position + 1) + ": "));
        int option = keyboard.readValidPositiveInteger("\n Ingrese el tipo de cuenta (0: cuenta corriente," +
                " 1: cuenta ahorros): ");
        account.setAccountType(getAccountType(option));
        account.setBalance(keyboard.readValidDouble("\n Ingrese el saldo de la cuenta: "));

        return account;
    }

    private AccountType getAccountType(int option) {
        switch (option) {
            case 0 -> {
                return AccountType.CuentaCorriente;
            }
            case 1 -> {
                return AccountType.CuentaAhorro;
            }
            default -> {
                System.out.println(" ¡Opción no disponible!");
                return AccountType.SinDefinir;
            }
        }
    }

    public void getMinBalanceUser() {
        System.out.println("\n  El usuario con menor saldo en la cuenta de ahorros es: "
                + bankController.getMinUserBalanceSavingAccount().getName());
    }

    public void getMaxBalanceUser() {
        System.out.println("\n  El usuario con mayor saldo en la cuenta corriente es: "
                + bankController.getMaxUserBalanceCurrentAccount().getName());
    }

    public void sortArrayUsers() {
        bankController.sortInsertionMethodUsers();
    }

    public void getMaxAccountBalance() {
        System.out.println("El tipo de cuenta con mayor saldo es: " + bankController.getMaxAccountBalance());
    }

    public void printUsers() {
        bankController.printUsers();
    }
}
